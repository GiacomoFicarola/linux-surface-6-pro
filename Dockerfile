FROM debian:stable-slim

# Install required packages
RUN apt-get update && \
    apt-get install -y \
    xmlstarlet \
    sbsigntool \
    jq \
    bc \
    kmod \
    cpio \
    curl \
    wget \
    git \
    zstd \
    build-essential \
    binutils-dev \
    libncurses5-dev \
    libssl-dev \
    ccache \
    bison \
    flex \
    libelf-dev \
    debhelper \
    rsync && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*