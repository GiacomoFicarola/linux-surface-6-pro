[![pipeline status](https://gitlab.com/GiacomoFicarola/linux-surface/badges/main/pipeline.svg)](https://gitlab.com/GiacomoFicarola/linux-surface/-/commits/main)            [![coverage report](https://gitlab.com/GiacomoFicarola/linux-surface/badges/main/coverage.svg)](https://gitlab.com/GiacomoFicarola/linux-surface/-/commits/main)            [![Latest Release](https://gitlab.com/GiacomoFicarola/linux-surface/-/badges/release.svg)](https://gitlab.com/GiacomoFicarola/linux-surface/-/releases)


**Small tutorial:**

Download the [key](https://gitlab.com/GiacomoFicarola/linux-surface/-/raw/main/Giacomo_Ficarola_Kernel.der) and enroll it.

Use this command on you're surface:
    `sudo mokutil --import Giacomo_Ficarola_Kernel.der`
You will be asked for a password, use what do you want, remember it.

Download the latest [release](https://gitlab.com/GiacomoFicarola/linux-surface/-/releases) of the kernel and install it with the command:
    `sudo dpkg -i linux-*`

Do the command:
    `sudo update-grub` and next `sudo reboot`

Verify that the kernel has been installed:
    `uname -a`

Now enjoy with my kernel, leave a star and stop by once a week to check for any new builds.
